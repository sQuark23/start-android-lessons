package com.squark23.lesson28;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by squark23 on 6/5/14.
 */
public class ViewActivity extends Activity {

    TextView tvView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view);

        tvView = (TextView) findViewById(R.id.tvView);

        Intent intent = getIntent();

        String fname = intent.getExtras().getString("fname");
        String lname = intent.getExtras().getString("lname");

        tvView.setText("Your name is: " + fname + " " + lname);
    }
}
