package com.squark23.lesson39;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

public class MyActivity extends Activity {

    final String LOG_TAG = "myLogs";

    final String DB_NAME = "staff";

    final int DB_VERSION = 2;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "--- Staff db v." + db.getVersion() + " ---");
        writeStaff(db);
        dbHelper.close();
    }

    private void writeStaff(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select * from people", null);
        logCursor(cursor, "Table people");
        cursor.close();

        cursor = db.rawQuery("select * from position", null);
        logCursor(cursor, "Table position");
        cursor.close();

        String sqlQuery = "select PL.name as Name, PS.name as Position, salary as Salary " +
                "from people as PL " +
                "inner join position as PS " +
                "on PL.posid = PS.id ";
        cursor = db.rawQuery(sqlQuery, null);
        logCursor(cursor, "Inner join");
        cursor.close();
    }

    private void logCursor(Cursor cursor, String title) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                Log.d(LOG_TAG, title + ". " + cursor.getCount() + " rows");
                StringBuilder sb = new StringBuilder();
                do {
                    sb.setLength(0);
                    for (String cn : cursor.getColumnNames()) {
                        sb.append(cn + " = " + cursor.getString(cursor.getColumnIndex(cn)) + "; ");
                    }
                    Log.d(LOG_TAG, sb.toString());
                } while (cursor.moveToNext());
            }
        } else
            Log.d(LOG_TAG, title + ". Cursor is null");
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(LOG_TAG, "--- onCreate() database ---");

            String[] peopleName = {"John", "Mary", "Peter", "Antony", "Darya", "Boris", "Constantine", "Dominic"};

            String[] peoplePositions = {"Programmer", "Secretary", "Programmer", "Programmer", "Secretary", "CEO", "Programmer", "Security"};

            ContentValues contentValues = new ContentValues();

            db.execSQL("create table people (" +
                    "id integer primary key autoincrement," +
                    "name text, position text);");

            for (int i = 0; i < peopleName.length; i++) {
                contentValues.clear();
                contentValues.put("name", peopleName[i]);
                contentValues.put("position", peoplePositions[i]);
                db.insert("people", null, contentValues);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(LOG_TAG, "--- onUpgrade database from " + oldVersion + " to " + newVersion + " version ---");

            if (oldVersion == 1 && newVersion == 2) {
                ContentValues contentValues = new ContentValues();

                int[] positionId = {1, 2, 3, 4};
                String[] positionName = {"CEO", "Programmer", "Secretary", "Security"};
                int[] positionSalary = {15000, 13000, 10000, 8000};

                db.beginTransaction();
                try {
                    db.execSQL("create table position (" +
                            "id integer primary key," +
                            "name text, salary integer);");

                    for (int i = 0; i < positionId.length; i++) {
                        contentValues.clear();
                        contentValues.put("id", positionId[i]);
                        contentValues.put("name", positionName[i]);
                        contentValues.put("salary", positionSalary[i]);
                        db.insert("position", null, contentValues);
                    }

                    db.execSQL("alter people add column posid integer;");

                    for (int i = 0; i < positionId.length; i++) {
                        contentValues.clear();
                        contentValues.put("posid", positionId[i]);
                        db.update("people", contentValues, "position = ?", new String[]{positionName[i]});
                    }

                    db.execSQL("create temporary table people_tmp (" +
                            "id integer, name text, position text, posid integer);");

                    db.execSQL("insert into people select id, name, posid from people_tmp;");
                    db.execSQL("drop table people_tmp");

                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            }
        }
    }
}
