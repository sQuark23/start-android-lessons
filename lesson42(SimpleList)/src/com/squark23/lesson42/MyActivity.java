package com.squark23.lesson42;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MyActivity extends Activity {

    String[] name = {"John", "Mary", "Peter", "Antony", "Darya", "Boris", "Constantine", "Dominic"};

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.my_list_item, name);

        lvMain.setAdapter(adapter);
    }
}
