package com.squark23.lesson33;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    EditText etText;
    Button btnSave;
    Button btnLoad;

    SharedPreferences shPref;

    final String SAVED_TEXT = "saved_text";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        etText = (EditText) findViewById(R.id.etText);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnLoad = (Button) findViewById(R.id.btnLoad);

        loadText();
    }

    public void onClickSaveBtn(View v) {
        // TODO something
        saveText();
    }

    public void onClickLoadBtn(View v) {
        // TODO something
        loadText();
    }

    private void saveText() {
        shPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = shPref.edit();
        editor.putString(SAVED_TEXT, etText.getText().toString());
        editor.commit();
        Toast.makeText(this, "Text saved", Toast.LENGTH_SHORT).show();
    }

    private void loadText() {
        shPref = getPreferences(MODE_PRIVATE);
        String savedText = shPref.getString(SAVED_TEXT, "");
        etText.setText(savedText);
        Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveText();
    }
}
