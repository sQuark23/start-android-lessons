package com.squark23.lesson32;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void onClickWebBtn(View v) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.github.com")));
    }
}
