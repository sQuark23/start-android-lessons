package com.squark23.lesson38;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

public class MyActivity extends Activity {

    final String LOG_TAG = "myLogs";

    DBHelper dbHelper;

    SQLiteDatabase db;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Log.d(LOG_TAG, "--- onCreate() Activity ---");
        dbHelper = new DBHelper(this);
        myActions();
    }

    private void myActions() {
/*        db = dbHelper.getWritableDatabase();
        delete(db, "mytable");
        insert(db, "mytable", "val1");
        read(db, "mytable");
        dbHelper.close();*/

/*        try {
            db = dbHelper.getWritableDatabase();
            delete(db, "mytable");

            db.beginTransaction();
            insert(db, "mytable", "val1");

            Log.d(LOG_TAG, "create DBHelper");
            DBHelper dbHelper2 = new DBHelper(this);
            Log.d(LOG_TAG, "get db");
            SQLiteDatabase db2 = dbHelper2.getWritableDatabase();
            read(db2, "mytable");
            dbHelper2.close();

            db.setTransactionSuccessful();
            db.endTransaction();

            read(db, "mytable");
            dbHelper.close();
        } catch (Exception e ) {
            Log.d(LOG_TAG, e.getClass() + " error: " + e.getMessage());
        }*/

/*        db = dbHelper.getWritableDatabase();
        SQLiteDatabase db2 = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "db = db2 - " + db.equals(db2));
        Log.d(LOG_TAG, "db open - " + db.isOpen() + ", db2 open - " + db2.isOpen());
        db2.close();
        Log.d(LOG_TAG, "db open - " + db.isOpen() + ", db2 open - " + db2.isOpen());*/
    }

    private void read(SQLiteDatabase db, String table) {
        Log.d(LOG_TAG, "Read table " + table);
        Cursor cursor = db.query(table, null, null, null, null, null, null, null);
        if (cursor != null ) {
            Log.d(LOG_TAG, "Records count = " + cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    Log.d(LOG_TAG, cursor.getString(cursor.getColumnIndex("val")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }

    private void insert(SQLiteDatabase db, String table, String value) {
        Log.d(LOG_TAG, "Insert in table " + table + " value = " + value);
        ContentValues contentValues = new ContentValues();
        contentValues.put("val", value);
        db.insert(table, null, contentValues);
    }

    private void delete(SQLiteDatabase db, String table) {
        Log.d(LOG_TAG, "Delete all from table " + table);
        db.delete(table, null, null);
    }

    class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context) {
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(LOG_TAG, "--- onCreate() database ---");

            db.execSQL("create table mytable (" +
                    "id integer primary key autoincrement, " + "val text);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
