package com.squark23.lesson27;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by squark23 on 6/5/14.
 */

public class InfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        Intent  intent = getIntent();

        String action = intent.getAction();

        String format = "";
        String textInfo = "";

        switch (action) {
            case "com.squark23.intent.action.showtime":
                format = "HH:mm:ss";
                textInfo = "Time: ";
                break;
            case "com.squark23.intent.action.showdate":
                format = "dd.MM.yyyy";
                textInfo = "Date: ";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        String dateTime = dateFormat.format(new Date(System.currentTimeMillis()));

        TextView tvDate = (TextView) findViewById(R.id.tvInfo);
        tvDate.setText(textInfo + dateTime);
    }
}
