package com.squark23.lesson30;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

/**
 * Created by squark23 on 6/5/14.
 */
public class AlignActivity extends Activity {
    Button btnCenter;
    Button btnLeft;
    Button btnRight;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.align);

        btnCenter = (Button) findViewById(R.id.btnCenter);
        btnRight = (Button) findViewById(R.id.btnRight);
        btnLeft = (Button) findViewById(R.id.btnLeft);

        intent = new Intent();
    }

    private void setRes() {
        setResult(RESULT_OK, intent);
        finish();
    }

    public void onClickCenterBtn(View v) {
        intent.putExtra("alignment", Gravity.CENTER);
        setRes();
    }

    public void onClickRightBtn(View v) {
        intent.putExtra("alignment", Gravity.RIGHT);
        setRes();
    }

    public void onClickLeftBtn(View v) {
        intent.putExtra("alignment", Gravity.LEFT);
        setRes();
    }
}
