package com.squark23.lesson30;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by squark23 on 6/5/14.
 */
public class ColorActivity extends Activity {

    Button btnRed;
    Button btnGreen;
    Button btnBlue;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.color);

        btnRed = (Button) findViewById(R.id.btnRed);
        btnBlue = (Button) findViewById(R.id.btnBlue);
        btnGreen = (Button) findViewById(R.id.btnGreen);

        intent = new Intent();
    }

    private void setRes() {
        setResult(RESULT_OK, intent);
        finish();
    }

    public void onClickRed(View v) {
        intent.putExtra("color", Color.RED);
        setRes();
    }

    public void onClickGreen(View v) {
        intent.putExtra("color", Color.GREEN);
        setRes();
    }

    public void onClickBlue(View v) {
        intent.putExtra("color", Color.BLUE);
        setRes();
    }
}
