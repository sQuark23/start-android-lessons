package com.squark23.lesson46;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

public class MyActivity extends Activity {

    final String LOG_TAG = "myLogs";

    ExpandableListView elvMain;
    AdapterHelper adapterHelper;
    SimpleExpandableListAdapter adapter;
    TextView tvInfo;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        tvInfo = ((TextView) findViewById(R.id.tvInfo));

        adapterHelper = new AdapterHelper(this);
        adapter = adapterHelper.getAdapter();

        elvMain = ((ExpandableListView) findViewById(R.id.elvMain));
        elvMain.setAdapter(adapter);

        elvMain.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Log.d(LOG_TAG, "onChildClick: groupPosition = " + groupPosition + "; childPosition = " + childPosition + "; id = " + id);
                tvInfo.setText(adapterHelper.getGroupChildText(groupPosition, childPosition));
                return false;
            }
        });

        elvMain.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Log.d(LOG_TAG, "onGroupClick: groupPosition = " + groupPosition + "; id = " + id);

                if (groupPosition == 1)
                    return true;

                return false;
            }
        });

        elvMain.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                Log.d(LOG_TAG, "onGroupCollapse: groupPosition = " + groupPosition);
                tvInfo.setText("Свернули " + adapterHelper.getGroupText(groupPosition));
            }
        });

        elvMain.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                Log.d(LOG_TAG, "onGroupExpand: groupPosition = " + groupPosition);
                tvInfo.setText("Развернули" + adapterHelper.getGroupText(groupPosition));
            }
        });

        elvMain.expandGroup(2);
    }
}
