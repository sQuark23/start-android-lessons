package com.squark23.lesson21;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by squark23 on 6/4/14.
 */
public class ActivityTwo extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two);
    }

}
