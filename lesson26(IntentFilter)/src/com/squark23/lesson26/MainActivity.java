package com.squark23.lesson26;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button btnTime = (Button) findViewById(R.id.btnTime);
        Button btnDate = (Button) findViewById(R.id.btnDate);
    }

    public void showTimeButtonClick(View v) {
        Intent intent = new Intent("com.squark23.intent.action.showtime");
        startActivity(intent);
    }

    public void showDateButtonClick(View v) {
        Intent intent = new Intent("com.squark23.intent.action.showdate");
        startActivity(intent);
    }
}
