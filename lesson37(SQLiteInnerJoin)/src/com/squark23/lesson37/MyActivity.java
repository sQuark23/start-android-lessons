package com.squark23.lesson37;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

public class MyActivity extends Activity {

    final String LOG_TAG = "myLogs";

    int[] positionId = {1, 2, 3, 4};

    String[] positionName = {"CEO", "Programmer", "Manager", "Security"};

    int[] positionSalary = {15000, 13000, 10000, 8000};

    String[] peopleName = {"John", "Mary", "Peter", "Antony", "Darya", "Boris", "Constantine", "Bill"};

    int[] peoplePositionId = {2, 3, 2, 2, 3, 1, 2, 4};

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor;

        // Выводим в лог данные по должностям
        Log.d(LOG_TAG, "--- Table position ---");
        cursor = db.query("position", null, null, null, null, null, null);
        logCursor(cursor);
        cursor.close();
        Log.d(LOG_TAG, "--- ---");

        Log.d(LOG_TAG, "--- Table people ---");
        cursor = db.query("people", null, null, null, null, null, null);
        logCursor(cursor);
        cursor.close();
        Log.d(LOG_TAG, "--- ---");


        // Выводим результат объединения
        // используя rawQuery
        Log.d(LOG_TAG, "--- INNER JOIN with rawQuery ---");
        String sqlQuery = "select PL.name as Name, PS.name as Position, salary as Salary " +
                "from people as PL " +
                "inner join position as PS " +
                "on PL.posid = PS.id " + "where salary > ?";
        cursor = db.rawQuery(sqlQuery, new String[]{"12000"});
        logCursor(cursor);
        cursor.close();
        Log.d(LOG_TAG, "--- ---");

        // Выводим результат объединения
        // используя query
        Log.d(LOG_TAG, "--- INNER JOIN with query ---");
        String table = "people as PL inner join position as PS on PL.posid = PS.id";
        String[] columns = {"PL.name as Name", "PS.name as Position", "salary as Salary"};
        String selection = "salary < ?";
        String[] selectionArgs = {"12000"};
        cursor = db.query(table, columns, selection, selectionArgs, null, null, null);
        logCursor(cursor);
        cursor.close();
        Log.d(LOG_TAG, "--- ---");

        dbHelper.close();
    }

    private void logCursor(Cursor cursor) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : cursor.getColumnNames()) {
                        str = str.concat(cn + " = " + cursor.getString(cursor.getColumnIndex(cn)) + ";");
                    }
                    Log.d(LOG_TAG, str);
                } while (cursor.moveToNext());
            }
        } else
            Log.d(LOG_TAG, "Cursor is null");
    }

    class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context) {
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(LOG_TAG, "-- onCreate() database");
            ContentValues contentValues = new ContentValues();

            db.execSQL("create table position (" + "id integer primary key," +
                    "name text," + "salary integer" + ");");

            for (int i = 0; i < positionId.length; i++) {
                contentValues.clear();
                contentValues.put("id", positionId[i]);
                contentValues.put("name", positionName[i]);
                contentValues.put("salary", positionSalary[i]);
                db.insert("position", null, contentValues);
            }

            db.execSQL("create table people (" +
                    "id integer primary key autoincrement," + "name text," +
                    "posid integer);");

            for (int i = 0; i < peopleName.length; i++) {
                contentValues.clear();
                contentValues.put("name", peopleName[i]);
                contentValues.put("posid", peoplePositionId[i]);
                db.insert("people", null, contentValues);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
